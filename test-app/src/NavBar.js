import React from 'react'

class NavBar extends React.Component{
    render() {
        return <header className="masthead mb-auto">
        <div className="inner">
          <h3 className="masthead-brand">Animal Safeguard</h3>
          <nav className="nav nav-masthead justify-content-center">
            <a className="nav-link active" href="index.html">Home</a>
            <a className="nav-link" href="animals.html">Animals</a>
            <a className="nav-link" href="countries.html">Countries</a>
            <a className="nav-link" href="ecosystems.html">Ecosystems</a>
            <a className="nav-link" href="about.html">About</a>
          </nav>
        </div>
      </header>
    }
}

export default NavBar