import React from 'react';
//import './App.css';
import './cover.css';


function App() {
  return (
    <body className="text-center">
    <div className="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
      <header className="masthead mb-auto">
        <div className="inner">
          <h3 className="masthead-brand">Animal Safeguard</h3>
          <nav className="nav nav-masthead justify-content-center">
            <a className="nav-link active" href="index.html">Home</a>
            <a className="nav-link" href="animals.html">Animals</a>
            <a className="nav-link" href="countries.html">Countries</a>
            <a className="nav-link" href="ecosystems.html">Ecosystems</a>
            <a className="nav-link" href="about.html">About</a>
          </nav>
        </div>
      </header>
      
      <main className="inner cover">
        <div className="opaque">
          <h1 className="cover-heading">Welcome to Animal Safeguard!</h1>
          <p className="lead">Our mission is to inform you about threatened and endangered animals. This
          website will give you vital information about what animals are at risk of extinction, where they
          can be found, and what their ecosystems are like. Our hope is that you use this information to
          make yourself aware of the threat to animals all around the world and to find ways to contribute
          to conservation efforts that will allow these animals to thrive for generations to come.</p>
        </div>
        <p className="lead">
          <a href="animals.html" className="btn btn-lg btn-secondary">Start exploring</a>
        </p>
      </main>
    </div>
    </body>
  );
}

export default App;
